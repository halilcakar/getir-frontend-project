import styled from 'styled-components';

export const Title = styled.h4`
  margin-top: 0;
  margin-bottom: 1rem;
  font-size: 1.4rem;
  font-weight: 400;
  line-height: 26px;
  letter-spacing: 0.25px;
  color: var(--color-black-500);
`;

export const ButtonGroup = styled.div`
  display: flex;
  gap: 0.5rem;
  margin-bottom: 1rem;
`;

interface IButton {
  readonly active?: boolean;
}
export const Button = styled.button<IButton>`
  background-color: var(
    ${(props) => (props.active ? '--color-primary' : '--color-btn-bg')}
  );
  color: var(
    ${(props) => (props.active ? '--color-white' : '--color-primary')}
  );
  border-radius: 2px;
  border: none;
  padding: 6px 16px;
  font-weight: 600;
`;

export const List = styled.div`
  display: grid;
  grid-gap: 24px;
  grid-template-columns: repeat(4, minmax(124px, 1fr));
`;
