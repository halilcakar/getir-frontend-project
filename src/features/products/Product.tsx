import { FC } from 'react';
import { Price } from '../../atomics/Globals';
import { addToCart } from '../cart/cartSlice';
import { useAppDispatch } from '../../app/hooks';
import { Product as IProduct } from './productsSlice';
import { Wrapper, PriceWrapper, Image, ImageBg, Title } from './Product.styles';
import { formatPrice } from '../../utils';

const Product: FC<{ product: IProduct }> = ({ product }) => {
  const dispatch = useAppDispatch();

  return (
    <Wrapper>
      <div>
        <ImageBg>
          <Image src="https://via.placeholder.com/300" alt={product.name} />
        </ImageBg>

        <PriceWrapper>
          <Price>₺ {formatPrice(product.price)}</Price>
        </PriceWrapper>

        <Title>{product.name}</Title>
      </div>
      <button onClick={() => dispatch(addToCart(product))}>Add</button>
    </Wrapper>
  );
};

export default Product;
