import {
  createAsyncThunk,
  createEntityAdapter,
  createSlice,
} from '@reduxjs/toolkit';
import { RootState } from '../../app/store';
import { fetchCompanies, setTags, ITag } from '../filters/filtersSlice';

export interface Product {
  tags: string[];
  name: string;
  slug: string;
  price: number;
  added: number;
  itemType: string;
  description: string;
  manufacturer: string;
}

const adapter = createEntityAdapter({
  selectId: (product: Product) => product.slug,
});

export const fetchProducts = createAsyncThunk(
  'products/fetch',
  async (_, { dispatch }) => {
    dispatch(await fetchCompanies());

    const data = await (await fetch('http://localhost:5000/items')).json();

    let tags: { [key: string]: number } = {};
    for (let i = 0; i < data.length; i++) {
      const element = data[i];
      element.tags.forEach((tag: string) => {
        if (!tags[tag]) {
          tags[tag] = 0;
        }
        tags[tag]++;
      });
    }
    dispatch(
      setTags(
        Object.entries(tags)
          .sort((a, z) => a[0].localeCompare(z[0]))
          .map((tag) => {
            return { value: tag[0], count: tag[1] } as ITag;
          })
      )
    );

    return data;
  }
);

const initialState = adapter.getInitialState({
  loading: false,
});

export const productsSlice = createSlice({
  name: 'products',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchProducts.pending, (state) => {
        state.loading = true;
      })
      .addCase(fetchProducts.fulfilled, (state, { payload }) => {
        state.loading = false;
        adapter.setAll(state, payload);
      })
      .addCase(fetchProducts.rejected, (state) => {
        state.loading = false;
        adapter.setAll(state, []);
      });
  },
});

export const selectProducts = (state: RootState) => state.products;

export default productsSlice.reducer;
