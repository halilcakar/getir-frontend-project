import { FC, useState, useEffect, useMemo } from 'react';
import { Background, Loader } from '../../atomics/Globals';
import { Title, ButtonGroup, Button, List } from './Products.styles';
import Product from './Product';
import { useAppDispatch, useAppSelector } from '../../app/hooks';
import { fetchProducts, selectProducts } from './productsSlice';
import { sortBy } from '../../utils';
import { selectFilters } from '../filters/filtersSlice';
import Pagination, { PageSize } from '../../components/Pagination';

const Products: FC = () => {
  const [type, setType] = useState('');
  const dispatch = useAppDispatch();
  const products = useAppSelector(selectProducts);
  const filters = useAppSelector(selectFilters);
  const [currentPage, setCurrentPage] = useState(1);

  useEffect(() => {
    dispatch(fetchProducts());
  }, [dispatch]);

  function toggleType(selectedType: string) {
    if (type === selectedType) {
      return setType('');
    }

    setType(selectedType);
  }

  let brandsFilter = filters.selectedBrands.length;
  let tagsFilter = filters.selectedTags.length;
  let selectedTags: string[] = filters.selectedTags.map((tag) => tag.value);
  const items = Object.values(products.entities)
    .filter((item) => {
      if (brandsFilter === 0) {
        return true;
      }
      return !!filters.selectedBrands.find(
        (brand) => brand.slug === item?.manufacturer
      );
    })
    .filter((item) => {
      if (tagsFilter === 0) {
        return true;
      }
      return item?.tags.some((tag) => selectedTags.includes(tag));
    })
    .filter((item) => item?.itemType.includes(type))
    .sort(sortBy[filters.sort]);

  const currentTableData = useMemo(() => {
    const firstPageIndex = (currentPage - 1) * PageSize;
    const lastPageIndex = firstPageIndex + PageSize;
    return items.slice(firstPageIndex, lastPageIndex);
  }, [currentPage, items]);

  if (!Object.values(products.entities).length) {
    return (
      <main data-testid="m-main">
        <Loader />
      </main>
    );
  }

  return (
    <main data-testid="m-main">
      <Title>Products</Title>

      <ButtonGroup>
        <Button onClick={() => toggleType('mug')} active={type === 'mug'}>
          mug
        </Button>
        <Button onClick={() => toggleType('shirt')} active={type === 'shirt'}>
          shirt
        </Button>
      </ButtonGroup>

      <Background padding={1.5}>
        <List>
          {currentTableData.map(
            (item) => item && <Product key={item.slug} product={item} />
          )}
        </List>

        <Pagination
          currentPage={currentPage}
          totalCount={items.length}
          pageSize={PageSize}
          onPageChange={(page: number) => setCurrentPage(page)}
        />
      </Background>
    </main>
  );
};

export default Products;
