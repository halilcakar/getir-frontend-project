import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  gap: 8px;

  button {
    border: none;
    border-radius: 2px;
    color: var(--color-white);
    background-color: var(--color-primary);
    transition: transform 200ms;
  }

  button:hover {
    transform: scale(1.05);
  }
`;

export const ImageBg = styled.div`
  width: 100%;
  display: grid;
  place-items: center;
  padding: 32px;
  border-radius: 12px;
  border: 1.17666px solid #f3f0fe;
`;

export const Image = styled.img`
  max-width: 92px;
  max-height: 92px;
  width: 100%;
  height: 100%;
  object-fit: contain;
`;

export const PriceWrapper = styled.div`
  margin-block: 7px;
`;

export const Title = styled.div`
  font-weight: 600;
  line-height: 20px;
  color: var(--color-gray-700);
`;
