import styled from 'styled-components';

export const Wrapper = styled.div`
  position: sticky;
  top: 1rem;
  min-width: 300px;
  padding: 0.5rem;
  border-radius: 2px;
  background-color: var(--color-primary);
  height: max-content;
`;

export const Inner = styled.div`
  display: flex;
  flex-direction: column;
  border-radius: inherit;
  background-color: var(--color-white);
  padding: 1.375rem;

  & > div:first-child {
    max-height: 296px;
    overflow-y: auto;
  }

  & > span {
    margin-top: 1.25rem;
    padding: 1rem 1.5rem;
    color: var(--color-primary);
    font-weight: 600;
    width: max-content;
    align-self: flex-end;
    border: 2px solid;
    border-radius: 2px;
  }
`;

export const CartItem = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;

  border-bottom: 1px solid hsl(0 0% 0% / 0.1);
  padding: 1.25rem 0;

  div:first-child {
    max-width: 160px;
  }

  button {
    border: none;
    color: var(--color-primary);
    font-size: 1.5rem;

    svg {
      width: 1.5rem;
      height: 1.5rem;
    }
  }

  button + span {
    font-weight: 700;
    padding: 0.5rem;
    color: var(--color-white);
    background-color: var(--color-primary);
  }
`;

export const Title = styled.div`
  text-overflow: ellipsis;
  overflow: hidden;
  white-space: nowrap;
`;
