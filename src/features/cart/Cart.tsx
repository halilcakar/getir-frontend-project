import { FC } from 'react';
import { useAppDispatch, useAppSelector } from '../../app/hooks';
import { Price } from '../../atomics/Globals';
import { formatPrice } from '../../utils';
import { Title, CartItem, Inner, Wrapper } from './Cart.styles';
import { decreaseItemCount, increaseItemCount, selectCart } from './cartSlice';

const Cart: FC = () => {
  const cart = useAppSelector(selectCart);
  const dispatch = useAppDispatch();

  if (!cart.items.length) {
    return (
      <Wrapper>
        <Inner>No item in the cart!</Inner>
      </Wrapper>
    );
  }

  return (
    <div>
      <Wrapper>
        <Inner>
          <div>
            {cart.items.map((item) => {
              return (
                <CartItem key={item.slug}>
                  <div>
                    <Title>{item.name}</Title>
                    <Price>₺{formatPrice(item.price)}</Price>
                  </div>
                  <div>
                    <button onClick={() => dispatch(decreaseItemCount(item))}>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke="currentColor"
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          strokeWidth={3}
                          d="M16 12H4"
                        />
                      </svg>
                    </button>
                    <span> {cart.itemsCount[item.slug]} </span>
                    <button onClick={() => dispatch(increaseItemCount(item))}>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke="currentColor"
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          strokeWidth={3}
                          d="M12 4v16m8-8H4"
                        />
                      </svg>
                    </button>
                  </div>
                </CartItem>
              );
            })}
          </div>

          <span>₺{formatPrice(cart.totalPrice)}</span>
        </Inner>
      </Wrapper>
    </div>
  );
};

export default Cart;
