import { createSlice } from '@reduxjs/toolkit';
import { RootState } from '../../app/store';
import { Product } from '../products/productsSlice';

export interface ICart {
  items: Product[];
  itemsCount: { [key: string]: number };
  totalPrice: number;
}

const initialState: ICart = {
  items: [],
  itemsCount: {},
  totalPrice: 0,
};

export const cartSlice = createSlice({
  name: 'cart',
  initialState,
  reducers: {
    emptyCart(state) {
      state.items = [];
      state.itemsCount = {};
      state.totalPrice = 0;
    },
    addToCart(state, { payload }) {
      if (typeof state.itemsCount[payload.slug] !== 'undefined') {
        state.itemsCount[payload.slug]++;
      } else {
        state.items.unshift(payload);
        state.itemsCount[payload.slug] = 1;
      }

      state.totalPrice = state.items.reduce((acc, child) => {
        return acc + child.price * state.itemsCount[child.slug];
      }, 0);
    },
    increaseItemCount(state, { payload }) {
      state.itemsCount[payload.slug]++;

      state.totalPrice = state.items.reduce((acc, child) => {
        return acc + child.price * state.itemsCount[child.slug];
      }, 0);
    },
    decreaseItemCount(state, { payload }) {
      state.itemsCount[payload.slug]--;

      if (state.itemsCount[payload.slug] === 0) {
        state.items = state.items.filter(
          (child) => child.slug !== payload.slug
        );
        delete state.itemsCount[payload.slug];
      }

      state.totalPrice = state.items.reduce((acc, child) => {
        return acc + child.price * state.itemsCount[child.slug];
      }, 0);
    },
  },
});

export const selectCart = (state: RootState) => state.cart;

export const { addToCart, increaseItemCount, decreaseItemCount, emptyCart } =
  cartSlice.actions;

export default cartSlice.reducer;
