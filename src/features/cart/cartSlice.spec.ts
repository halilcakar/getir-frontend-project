import { Product } from '../products/productsSlice';
import cartReducer, {
  addToCart,
  increaseItemCount,
  decreaseItemCount,
  ICart,
  emptyCart,
} from './cartSlice';

const items = [
  {
    tags: ['Trees'],
    price: 10.99,
    name: 'Handcrafted Trees Mug',
    description:
      'enim corporis voluptatibus laudantium possimus alias dolorem voluptatem similique aut aliquam voluptatem voluptatem omnis id consequatur',
    slug: 'Handcrafted-Trees-Mug',
    added: 1485723766805,
    manufacturer: 'OHara-Group',
    itemType: 'mug',
  },
  {
    tags: ['Beach', 'Ocean', 'Water'],
    price: 19.99,
    name: 'Rustic Beach Mug',
    description:
      'totam at veritatis eligendi assumenda ex quia praesentium quibusdam ducimus',
    slug: 'Rustic-Beach-Mug',
    added: 1481573896833,
    manufacturer: 'Sipes-Inc',
    itemType: 'mug',
  },
] as Product[];

describe('cart reducer', () => {
  const initialState: ICart = {
    items: [],
    itemsCount: {},
    totalPrice: 0,
  };

  it('should handle initial state', () => {
    expect(cartReducer(undefined, { type: 'unknown' })).toEqual(initialState);
  });

  it('should add item to cart', () => {
    let product = items[0];
    const state = cartReducer(initialState, addToCart(product));

    expect(state.totalPrice).toBe(product.price);
    expect(state.items).toHaveLength(1);
    expect(state.itemsCount[product.slug]).toBe(1);
  });

  it('should increment an items count in the state', () => {
    let product = items[0];
    const initialState: ICart = {
      items: [product],
      itemsCount: {
        [product.slug]: 1,
      },
      totalPrice: product.price,
    };
    const state = cartReducer(initialState, increaseItemCount(product));

    expect(state.totalPrice).toBe(product.price * 2);
    expect(state.items).toHaveLength(1);
    expect(state.itemsCount[product.slug]).toBe(2);
  });

  it('should decrement an items count in the state', () => {
    let product = items[1];
    let totalPrice = items.reduce((acc, item) => acc + item.price, 0);
    const initialState: ICart = {
      items: items,
      itemsCount: {
        [items[0].slug]: 1,
        [items[1].slug]: 1,
      },
      totalPrice,
    };
    const state = cartReducer(initialState, decreaseItemCount(product));

    expect(+state.totalPrice).toBe(+(totalPrice - product.price).toFixed(2));
    expect(state.items).toHaveLength(1);
    expect(state.itemsCount[product.slug]).toBeUndefined();
  });

  it('should empty card when called', () => {
    let totalPrice = items.reduce((acc, item) => acc + item.price, 0);
    const initialState: ICart = {
      items: items,
      itemsCount: {
        [items[0].slug]: 1,
        [items[1].slug]: 1,
      },
      totalPrice,
    };
    const state = cartReducer(initialState, emptyCart());
    expect(state.totalPrice).toBe(0);
    expect(state.items).toHaveLength(0);
    expect(Object.keys(state.itemsCount)).toHaveLength(0);
  });
});
