import { FC } from 'react';
import Sorting from '../../components/Sorting';
import Brands from '../../components/Brands';
import Tags from '../../components/Tags';

const Filters: FC = () => {
  return (
    <div>
      <Sorting />
      <Brands />
      <Tags />
    </div>
  );
};

export default Filters;
