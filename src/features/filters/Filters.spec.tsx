import React from 'react';
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { store } from '../../app/store';
import Filters from './Filters';

it('renders filters', () => {
  render(
    <Provider store={store}>
      <Filters />
    </Provider>
  );

  expect(screen.getByText(/Sorting/i)).toBeInTheDocument();
  expect(screen.getByText(/Brands/i)).toBeInTheDocument();
  expect(screen.getByText(/Tags/i)).toBeInTheDocument();
});
