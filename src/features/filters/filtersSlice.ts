import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { RootState } from '../../app/store';

export interface ITag {
  value: string;
  count: number;
}

export interface IBrand {
  slug: string;
  name: string;
  address: string;
  city: string;
  state: string;
  zip: string;
  account: number;
  contact: string;
}

export interface IFilters {
  sort: 'new-to-old' | 'old-to-new' | 'price-low-to-high' | 'price-high-to-low';
  brands: IBrand[];
  selectedBrands: IBrand[];
  tags: ITag[];
  selectedTags: ITag[];
  loading: boolean;
}

const initialState: IFilters = {
  sort: 'new-to-old',
  brands: [],
  selectedBrands: [],
  tags: [],
  selectedTags: [],
  loading: false,
};

export const fetchCompanies = createAsyncThunk(
  'filters/fetchCompanies',
  async () => {
    return await (await fetch('http://localhost:5001/companies')).json();
  }
);

export const filtersSlice = createSlice({
  name: 'filters',
  initialState,
  reducers: {
    setSort(state, action) {
      state.sort = action.payload;
    },
    setBrands(state, action) {
      state.brands = action.payload;
    },
    setTags(state, action) {
      state.tags = action.payload;
    },
    toggleBrand(state, { payload }) {
      let brand = state.selectedBrands.find(
        (child) => child.slug === payload.slug
      );
      if (brand) {
        state.selectedBrands = state.selectedBrands.filter(
          (child) => child.slug !== payload.slug
        );
      } else {
        state.selectedBrands.push(payload);
      }
    },
    toggleTag(state, { payload }) {
      let tag = state.selectedTags.find(
        (child) => child.value === payload.value
      );
      if (tag) {
        state.selectedTags = state.selectedTags.filter(
          (tag) => tag.value !== payload.value
        );
      } else {
        state.selectedTags.push(payload);
      }
    },
    showAllBrands(state) {
      state.selectedBrands = [];
    },
    showAllTags(state) {
      state.selectedTags = [];
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchCompanies.pending, (state) => {
        state.loading = true;
      })
      .addCase(fetchCompanies.fulfilled, (state, { payload }) => {
        state.loading = false;
        state.brands = payload.sort((a: IBrand, z: IBrand) =>
          a.name.localeCompare(z.name)
        );
      })
      .addCase(fetchCompanies.rejected, (state) => {
        state.loading = false;
        state.brands = [];
      });
  },
});

export const selectFilters = (state: RootState) => state.filters;

export const {
  setSort,
  setBrands,
  setTags,
  toggleBrand,
  showAllBrands,
  toggleTag,
  showAllTags,
} = filtersSlice.actions;

export default filtersSlice.reducer;
