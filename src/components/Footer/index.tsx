import React from 'react';
import { Wrapper } from './Footer.styles';

const Footer = () => {
  return (
    <Wrapper data-testid="m-footer">
      ©{new Date().getFullYear()} Market • Privacy Policy
    </Wrapper>
  );
};

export default Footer;
