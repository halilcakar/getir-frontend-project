import styled from 'styled-components';

export const Wrapper = styled.footer`
  width: 100%;
  display: flex;
  justify-content: center;
  color: var(--color-primary);
  margin-block: 40px;
`;
