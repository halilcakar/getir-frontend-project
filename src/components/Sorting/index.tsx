import { FC } from 'react';
import { useAppDispatch, useAppSelector } from '../../app/hooks';
import Filter from '../../atomics/Filter';
import Radio from '../../atomics/Radio';
import { selectFilters, setSort } from '../../features/filters/filtersSlice';

const Sorting: FC = () => {
  const filters = useAppSelector(selectFilters);
  const dispatch = useAppDispatch();

  return (
    <Filter title="Sorting">
      <Radio
        title="New to old"
        checked={filters.sort === 'new-to-old'}
        onChange={() => dispatch(setSort('new-to-old'))}
      />
      <Radio
        title="Old to new"
        checked={filters.sort === 'old-to-new'}
        onChange={() => dispatch(setSort('old-to-new'))}
      />
      <Radio
        title="Price high to low"
        checked={filters.sort === 'price-high-to-low'}
        onChange={() => dispatch(setSort('price-high-to-low'))}
      />
      <Radio
        title="Price low to high"
        checked={filters.sort === 'price-low-to-high'}
        onChange={() => dispatch(setSort('price-low-to-high'))}
      />
    </Filter>
  );
};

export default Sorting;
