import styled, { css } from 'styled-components';

export const Wrapper = styled.ul<{ justify: string }>`
  display: flex;
  justify-content: ${(props) => props.justify};
  list-style-type: none;
  margin-block: 32px;
  padding-inline: 36px;
`;

interface IPageNumber {
  active?: boolean;
  disabled?: boolean;
}
export const PageNumber = styled.li<IPageNumber>`
  padding: 12px;
  border-radius: 2px;

  :hover {
    cursor: pointer;
    color: var(--color-primary);
  }

  pointer-events: ${(props) => (props.disabled || false ? 'none' : 'revert')};

  ${(props) =>
    props.active &&
    css`
      color: var(--color-white);
      background-color: var(--color-primary);
    `};
`;

export const LeftArrow = styled(PageNumber)`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  align-items: center;
  padding: 6px;
`;

export const RightArrow = styled(LeftArrow)``;
