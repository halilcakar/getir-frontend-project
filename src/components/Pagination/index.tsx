import { FC } from 'react';
import { DOTS, usePagination } from '../../hooks/usePagination';
import {
  LeftArrow,
  PageNumber,
  RightArrow,
  Wrapper,
} from './Pagination.styles';

export interface IPagination {
  onPageChange: Function;
  totalCount: number;
  siblingCount?: number;
  currentPage: number;
  pageSize: number;
}

export const PageSize = 16;

const Pagination: FC<IPagination> = (props) => {
  const {
    onPageChange,
    totalCount,
    currentPage,
    pageSize,
    siblingCount = 1,
  } = props;

  const paginationRange = usePagination({
    currentPage,
    totalCount,
    siblingCount,
    pageSize,
  });

  if (
    typeof paginationRange === 'undefined' ||
    currentPage === 0 ||
    paginationRange.length < 2
  ) {
    return null;
  }

  const onNext = () => {
    onPageChange(currentPage + 1);
  };

  const onPrevious = () => {
    onPageChange(currentPage - 1);
  };

  let lastPage = paginationRange[paginationRange.length - 1];
  let hasDots = paginationRange.some((number) => number === DOTS);

  return (
    <Wrapper justify={hasDots ? 'space-evenly' : 'center'}>
      {hasDots && (
        <LeftArrow disabled={currentPage === 1} onClick={onPrevious}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={1}
              d="M11 17l-5-5m0 0l5-5m-5 5h12"
            />
          </svg>
          <div>Prev</div>
        </LeftArrow>
      )}
      {paginationRange.map((pageNumber, index) => {
        if (pageNumber === DOTS) {
          return <PageNumber key={index}>&#8230;</PageNumber>;
        }
        return (
          <PageNumber
            key={index}
            active={pageNumber === currentPage}
            onClick={() => onPageChange(pageNumber)}
          >
            {pageNumber}
          </PageNumber>
        );
      })}
      {hasDots && (
        <RightArrow disabled={currentPage === lastPage} onClick={onNext}>
          <div>Next</div>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={1}
              d="M13 7l5 5m0 0l-5 5m5-5H6"
            />
          </svg>
        </RightArrow>
      )}
    </Wrapper>
  );
};

export default Pagination;
