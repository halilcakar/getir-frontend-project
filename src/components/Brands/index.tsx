import { useState, FC, ChangeEvent } from 'react';
import { useAppDispatch, useAppSelector } from '../../app/hooks';
import Checkbox from '../../atomics/Checkbox';
import Filter from '../../atomics/Filter';
import { ListWrapper, Loader } from '../../atomics/Globals';
import SearchInput from '../../atomics/SearchInput';
import {
  selectFilters,
  showAllBrands,
  toggleBrand,
} from '../../features/filters/filtersSlice';

const Brands: FC = () => {
  const [search, setSearch] = useState('');
  const filters = useAppSelector(selectFilters);
  const dispatch = useAppDispatch();

  return (
    <Filter title="Brands">
      <SearchInput
        type="search"
        placeholder="Search brand"
        value={search}
        onChange={(e: ChangeEvent<HTMLInputElement>) =>
          setSearch(e.target.value)
        }
      />

      <ListWrapper>
        {search === '' && (
          <Checkbox
            title="All"
            key={-1}
            checked={filters.selectedBrands.length === 0}
            onChange={() => dispatch(showAllBrands())}
          />
        )}
        {!filters.brands.length ? (
          <Loader size={25} />
        ) : (
          filters.brands
            .filter((child) =>
              child.name.toLowerCase().includes(search.toLowerCase())
            )
            .map((child) => (
              <Checkbox
                key={child.slug}
                title={child.name}
                checked={filters.selectedBrands.includes(child)}
                onChange={() => dispatch(toggleBrand(child))}
              />
            ))
        )}
      </ListWrapper>
    </Filter>
  );
};

export default Brands;
