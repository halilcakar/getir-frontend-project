import { useState, FC, ChangeEvent } from 'react';
import { useAppDispatch, useAppSelector } from '../../app/hooks';
import Checkbox from '../../atomics/Checkbox';
import Filter from '../../atomics/Filter';
import { ListWrapper, Loader } from '../../atomics/Globals';
import SearchInput from '../../atomics/SearchInput';
import {
  selectFilters,
  showAllTags,
  ITag,
  toggleTag,
} from '../../features/filters/filtersSlice';

const Tags: FC = () => {
  const [search, setSearch] = useState('');
  const filters = useAppSelector(selectFilters);
  const dispatch = useAppDispatch();

  function isTagSelected(tag: ITag): boolean {
    return !!filters.selectedTags.find((c) => c.value === tag.value);
  }

  const total = filters.tags.reduce((acc, tag) => {
    return acc + tag.count;
  }, 0);

  return (
    <Filter title="Tags">
      <SearchInput
        type="search"
        placeholder="Search tag"
        value={search}
        onChange={(e: ChangeEvent<HTMLInputElement>) =>
          setSearch(e.target.value)
        }
      />

      <ListWrapper>
        {search === '' && (
          <Checkbox
            title="All"
            count={total}
            key={-1}
            checked={filters.selectedTags.length === 0}
            onChange={() => dispatch(showAllTags())}
          />
        )}
        {!filters.tags.length ? (
          <Loader size={25} />
        ) : (
          filters.tags
            .filter((child) =>
              child.value.toLowerCase().includes(search.toLowerCase())
            )
            .map((child) => (
              <Checkbox
                count={child.count}
                key={child.value}
                title={child.value}
                checked={isTagSelected(child)}
                onChange={() => dispatch(toggleTag(child))}
              />
            ))
        )}
      </ListWrapper>
    </Filter>
  );
};

export default Tags;
