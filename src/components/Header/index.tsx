import { Wrapper, TotalPrice } from './Header.styles';
import Brand from '../../assets/brand.svg';
import Basket from '../../assets/basket.svg';
import { useAppSelector } from '../../app/hooks';
import { selectCart } from '../../features/cart/cartSlice';
import { formatPrice } from '../../utils';

const Header = () => {
  const cart = useAppSelector(selectCart);

  return (
    <Wrapper data-testid="m-header">
      <img src={Brand} alt="Brand Logo" />

      {cart.totalPrice > 0 && (
        <TotalPrice>
          <img src={Basket} alt="basket" />
          <div>₺ {formatPrice(cart.totalPrice)}</div>
        </TotalPrice>
      )}
    </Wrapper>
  );
};

export default Header;
