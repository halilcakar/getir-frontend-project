import styled from "styled-components";

export const Wrapper = styled.header`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  height: 5rem;
  background-color: var(--color-primary);
`;

export const TotalPrice = styled.div`
  position: absolute;
  width: 8rem;
  height: 100%;
  right: 6.5rem;

  display: flex;
  justify-content: center;
  align-items: center;
  color: var(--color-white);
  background-color: var(--color-secondary);

  & > div {
    margin-left: 0.5rem;
    font-weight: 600;
  }
`;
