import styled from 'styled-components';

export const Wrapper = styled.label`
  display: flex;
  cursor: pointer;
  position: relative;
  overflow: hidden;
  margin-top: 12px;
  width: max-content;

  &:first-child {
    margin-top: 0;
  }

  & input:checked + span:before {
    color: var(--color-white);
    background-color: var(--color-primary);
  }
`;

export const Checkmark = styled.span`
  display: flex;
  align-items: center;
  padding: 0.375rem;
  transition: 250ms ease;

  &:before {
    display: flex;
    justify-content: center;
    flex-shrink: 0;
    content: '✔';
    color: var(--color-white);
    background-color: var(--color-white);
    width: 1.5rem;
    height: 1.5rem;
    margin-right: 0.375rem;
    transition: 0.25s ease;
    border-radius: 2px;
    box-shadow: 0px 1px 7px rgba(93, 56, 192, 0.4);
  }

  & > span {
    color: var(--color-black-300);
    margin-left: 4px;
  }
`;
