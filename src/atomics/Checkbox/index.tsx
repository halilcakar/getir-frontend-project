import { FC, ChangeEventHandler } from 'react';
import { Input } from '../Globals';
import { Checkmark, Wrapper } from './Checkbox.styles';

interface ICheckbox {
  title: string;
  checked?: boolean;
  onChange: ChangeEventHandler;
  count?: number;
}

const Checkbox: FC<ICheckbox> = ({
  title,
  onChange,
  checked = false,
  count = 0,
}) => {
  return (
    <Wrapper>
      <Input onChange={onChange} checked={checked} type="checkbox" />
      <Checkmark>
        {title}
        {count > 0 && <span>({count})</span>}
      </Checkmark>
    </Wrapper>
  );
};

export default Checkbox;
