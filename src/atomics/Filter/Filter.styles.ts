import styled from 'styled-components';

export const FilterHeader = styled.h5`
  margin-top: 0;
  margin-bottom: 12px;

  &:not(:first-child) {
    margin-block: 12px;
  }
`;
