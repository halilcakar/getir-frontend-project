import { FC } from 'react';
import { Background } from '../Globals';
import { FilterHeader } from './Filter.styles';

interface IFilter {
  title: string;
}

const Filter: FC<IFilter> = ({ title, children }) => {
  return (
    <>
      <FilterHeader>{title}</FilterHeader>
      <Background padding={1.72}>{children}</Background>
    </>
  );
};

export default Filter;
