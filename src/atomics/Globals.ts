import styled from 'styled-components';

export const MainWrapper = styled.section`
  display: grid;
  grid-gap: 14px;
  grid-template-columns: 1fr 2fr 1fr;
  margin: 2.375rem 6.5rem 0 6.5rem;
`;

export const ListWrapper = styled.div`
  overflow-y: auto;
  max-height: 142px;
  margin-top: 1rem;
`;

interface IBackground {
  padding?: number;
}
export const Background = styled.div<IBackground>`
  border-radius: 2px;
  background-color: var(--color-white);
  padding: ${(props) => props.padding || 1}rem;
`;

export const Price = styled.span`
  color: var(--color-primary);
  font-weight: 600;
`;

export const Input = styled.input`
  position: absolute;
  left: -9999px;
`;

interface ILoader {
  size?: number;
}
export const Loader = styled.div<ILoader>`
  border: ${(props) => (props.size || 50) / 10}px solid var(--color-black-300);
  border-block: ${(props) => (props.size || 50) / 10}px solid
    var(--color-black-500);
  border-radius: 50%;
  width: ${(props) => props.size || 50}px;
  height: ${(props) => props.size || 50}px;
  margin: 20px auto;
  animation: spin 1.5s linear infinite;
  @keyframes spin {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }
`;
