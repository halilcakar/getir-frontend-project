import styled from 'styled-components';

export const Wrapper = styled.label`
  display: flex;
  cursor: pointer;
  position: relative;
  overflow: hidden;
  width: max-content;

  & input:checked + span:before {
    color: var(--color-primary);
  }
`;

export const Checkmark = styled.span`
  display: flex;
  align-items: center;
  padding: 0.375rem;
  border-radius: 99em;
  transition: 250ms ease;

  &:before {
    display: flex;
    justify-content: center;
    flex-shrink: 0;
    content: '✔';
    color: var(--color-white);
    background-color: var(--color-white);
    width: 1.5rem;
    height: 1.5rem;
    border-radius: 50%;
    margin-right: 0.375rem;
    transition: 0.25s ease;
    box-shadow: inset 0 0 0 0.125rem var(--color-primary);
  }
`;
