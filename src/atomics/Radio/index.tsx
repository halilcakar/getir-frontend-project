import { FC } from 'react';
import { ChangeEventHandler } from 'react';
import { Input } from '../Globals';
import { Checkmark, Wrapper } from './Radio.styles';

interface IRadio {
  title: string;
  checked?: boolean;
  onChange: ChangeEventHandler;
}

const Radio: FC<IRadio> = ({ title, onChange, checked = false }) => {
  return (
    <Wrapper>
      <Input onChange={onChange} checked={checked} type="radio" />
      <Checkmark>{title}</Checkmark>
    </Wrapper>
  );
};

export default Radio;
