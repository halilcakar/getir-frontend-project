import styled from 'styled-components';

export const Input = styled.input`
  width: 100%;
  border-radius: 2px;
  box-sizing: border-box;
  border: 2px solid var(--color-black-100);
  padding: 12px 16px;

  &:focus {
    outline: none;
  }

  ::placeholder {
    color: var(--color-black-300);
  }
`;
