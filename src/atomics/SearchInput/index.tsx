import { FC, InputHTMLAttributes } from 'react';
import { Input } from './SearchInput.styles';

interface ISearchInput extends InputHTMLAttributes<HTMLInputElement> {}

const SearchInput: FC<ISearchInput> = ({ ...attr }) => {
  return <Input {...attr} type="search" />;
};

export default SearchInput;
