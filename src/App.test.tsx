import React from 'react';
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { store } from './app/store';
import App from './App';

it('renders part of app correctly', () => {
  const { getByTestId } = render(
    <Provider store={store}>
      <App />
    </Provider>
  );

  expect(getByTestId('m-header')).toBeInTheDocument();
  expect(getByTestId('m-main')).toBeInTheDocument();
  expect(getByTestId('m-footer')).toBeInTheDocument();
});
