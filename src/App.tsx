import { MainWrapper } from './atomics/Globals';
import Header from './components/Header';
import Filters from './features/filters/Filters';
import Products from './features/products/Products';
import Cart from './features/cart/Cart';
import Footer from './components/Footer';

function App() {
  return (
    <>
      <Header />
      <MainWrapper>
        <Filters />
        <Products />
        <Cart />
      </MainWrapper>
      <Footer />
    </>
  );
}

export default App;
