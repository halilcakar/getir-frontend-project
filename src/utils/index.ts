import { Product } from '../features/products/productsSlice';

export const formatPrice = (price: number) => {
  return price.toFixed(2);
};

export const sortBy = {
  'new-to-old': (a: Product | undefined, z: Product | undefined) => {
    if (typeof a !== 'undefined' && typeof z !== 'undefined') {
      return z.added - a.added;
    }
    return 0;
  },
  'old-to-new': (a: Product | undefined, z: Product | undefined) => {
    if (typeof a !== 'undefined' && typeof z !== 'undefined') {
      return a.added - z.added;
    }
    return 0;
  },
  'price-low-to-high': (a: Product | undefined, z: Product | undefined) => {
    if (typeof a !== 'undefined' && typeof z !== 'undefined') {
      return a.price - z.price;
    }
    return 0;
  },
  'price-high-to-low': (a: Product | undefined, z: Product | undefined) => {
    if (typeof a !== 'undefined' && typeof z !== 'undefined') {
      return z.price - a.price;
    }
    return 0;
  },
};
